/*global $,document*/

function News() {
    this.news_data = [];
}

News.prototype.init = function() {
    var $this = this, news_page = $('.news').length;

    $.ajax({
        url: 'http://www.army.mil/api/packages/getpackagesbykeywords',
        type: 'get',
        data: 'keywords=target_bestwarrior&count=25&limit=1',
        dataType: 'json',
        success: function(data) {
            if (data) {
                $this.news_data = data.slice(0);

                // this file is used on news and archive pages
                if (news_page) {
                    $this.build_news();
                } else {
                    $this.build_archive();
                }
            }
        },
        error: function(jqXHR, textStatus) {
            $('.content-container').prepend(
                '<p>Error retrieving news data: ' + textStatus + '</p>'
            );
        }
    });
};

News.prototype.build_news = function() {
    var blocks = $('.story-block'), i = 0, $this = this;

    for (i; i < blocks.length; i++) {

        $(blocks[i]).find('div a').prop({
            'href': $this.news_data[i].page_url
        });

        $(blocks[i]).find('.right-text a').html(
            $this.news_data[i].title
        );

        $(blocks[i]).find('div.right-text p').html(
            $this.news_data[i].description.substring(0, 200) +
            ' <a target="_blank"href="' +
            $this.news_data[i].page_url +
            '">...MORE</a>'
        );

        $(blocks[i]).find('.left-img img').prop({
            'src': $this.news_data[i].images[0].url_size3,
            'alt': $this.news_data[i].images[0].alt
        });
    }
};

News.prototype.build_archive = function() {
    var i = 0, $this = this;

    for (i; i < $this.news_data.length; i++) {
        $('#archive-links').append(
            '<li><a href="' + $this.news_data[i].page_url + '">' +
            $this.news_data[i].title + '</a></li>'
        );
    }
};

$(document).ready(function() {
    var news = new News();
    news.init();
});
